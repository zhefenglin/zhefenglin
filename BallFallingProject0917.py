# Mini Project 1
import math
import matplotlib.pyplot as plt

# global variables
gg = 9.81 # m/s**2
dT = 1e-4 # s
TT = 0 # s

# Model
ddz = -gg

# initial value
zz = 0 # m
xx = 0 # m

vv0 = 5 # m/s
aa0 = 45 # degree (From 0 to 180)

dx = vv0 * math.cos(math.radians(aa0))
dz = vv0 * math.sin(math.radians(aa0))

# initial state
X_pos = []
Z_pos = []
ds_BUF=[]
ds_HEAD=0
ds_TAIL=0
ds_ISEMPTY=True
ds_SIZE=1000

# initiate
def ds_Init():
        global ds_HEAD, ds_TAIL, ds_ISEMPTY
        ds_HEAD=0
        ds_TAIL=0
        ds_ISEMPTY=True
        for ii in range(0,ds_SIZE):
                ds_BUF.append(-1)
# add element as tuple to a list
def ds_add(datum):
        global ds_HEAD, ds_TAIL, ds_ISEMPTY
        ds_BUF[ds_HEAD]=datum
        if ds_HEAD==ds_SIZE-1:
                ds_HEAD=0
                ds_TAIL=0
        else:
                if ds_HEAD==ds_TAIL and not ds_ISEMPTY:
                    ds_HEAD=ds_HEAD+1
                    ds_TAIL=ds_HEAD
                else:
                        ds_HEAD=ds_HEAD+1
        ds_ISEMPTY=False

def ds_GetIndex(index):
        return ds_BUF[(index+ds_TAIL)%ds_SIZE]

def ds_GetNewest():
        return ds_BUF[ds_HEAD-1]

def ds_GetOldest():
        return ds_BUF[ds_TAIL]

# devide the elements in the tuple
def data_process(data):
        T=[]
        X=[]
        Z=[]
        V_x=[]
        V_z=[]
        for status in data:
                T.append(status[0])
                X.append(status[1])
                Z.append(status[2])
                V_x.append(status[3])
                V_z.append(status[4])
        return T, X, Z, V_x, V_z

# find median
def find_median(A):
        if len(A) % 2 == 1:
                A_median=A[(len(A)-1)/2]
        else:
                A_median=(A[(len(A)/2-1)]+A[len(A)/2])/2
        return A_median

# find standard deviation
def find_std(A,A_average):
        sqr_sum=0
        for i in A:
                sqr_sum=sqr_sum+(i-A_average)**2
        return (sqr_sum/len(A))**0.5
 
 # find the max, min, mean, median and stdDev       
def stat_comp(A):
        A.sort()
        A_max = max(A)
        A_min = min(A)
        A_average = sum(A)/len(A)
        A_median = find_median(A)
        A_stdDev = find_std(A,A_average)
        A_stat = [A_max,A_min,A_average,A_median,A_stdDev]
        return A_stat
       


# solver
def main():
        global TT, zz, xx, dx, dz, ddz, dT
        ds_Init()
        while zz >= 0 :

                zz = zz + dz * dT + 0.5 * ddz * dT ** 2
                xx = dx * dT + xx
                dz = dz + ddz * dT
                TT = TT + dT
                ds_add((TT, xx, zz, dx, dz))
                if zz < 0:
                    zz = 0
                    dz = -dz
                if TT > 10:
                    break

main()

T, X, Z, V_x, V_z = data_process(ds_BUF)

# plot the last 1000 points
X1=X[ds_TAIL:1000]
Z1=Z[ds_TAIL:1000]
X2=X[0:ds_HEAD]
Z2=Z[0:ds_HEAD]
X_pos=X1+X2
Z_pos=Z1+Z2
plt.plot(X_pos,Z_pos)
plt.xlabel('X axis')
plt.ylabel('Z axis')
plt.title('projectile (bouncing when hits the ground)')

# find the statastical value of data


X_stat = stat_comp(X)
Z_stat = stat_comp(Z)
V_x_stat = stat_comp(V_x)
V_z_stat = stat_comp(V_z)
# Note that now the order of X, Z, V_x and V_z are changed!!

#print data
print 'The maximun, minimum, mean, median and standard deviation of X are', X_stat
print 'The maximun, minimum, mean, median and standard deviation of Z are', Z_stat
print 'The maximun, minimum, mean, median and standard deviation of dx are', V_x_stat
print 'The maximun, minimum, mean, median and standard deviation of dz are' , V_z_stat
print 'The newest data is', ds_GetNewest()
print 'The oldest data is', ds_GetOldest()

# show the plot
plt.show()