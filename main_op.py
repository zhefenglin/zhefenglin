import circle_class as cc
import triangle_class as tc

#instantiate a circle and a triangle object
circ = cc.Circle()
tri = tc.Triangle()

#initialize attributes
tri.height = 1
tri.width = 0.5
circ.radius = 2

# compute / print
print 'area of the circle is',circ.getArea()
print 'area of the triangle is', tri.getArea()