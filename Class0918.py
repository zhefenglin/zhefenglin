import numpy as np

#Create a 9x1 vector of numbers 0 to 3, make it into a 3x3 array
a = np.array(range(0,9))
print a
A = a.reshape((3,3))
print A
print A.shape
#rotate top row to bottom
B = np.roll(A,3,0)
print B
#find entries bigger than three
mask = (A>3)&(A<7)
print mask.astype(np.int)
#Create a matrix of the first and 3rd rows
C = A[[0,2],:]
print C
#matrix multiplication
print np.dot(C,A)
print C.dot(A)
#componentwise multiplication
print np.multiply(A,mask)
print C[0,0]