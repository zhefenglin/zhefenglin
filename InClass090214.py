print 'I am not a robot'
def ps():
	print 'something'

ps()
#comment

def foo(param,*nums,**dict):
	print param
	print nums
	print dict

foo(1,2,3,4,5,cell=5121111111,home=512222222)

#lists
icebucket_nominees = ['MIT Rafael Reif','UT Powels','Harvard Faust']
print 'num of nominees', len(icebucket_nominees)
ii = 1
for nominee in icebucket_nominees:
	print 'nominee',ii,'is',nominee
	ii+=1
icebucket_nominees.sort()
print 'sorted list', icebucket_nominees


#create a list of numbers
#sort it
#print

numberlist = [52, 58, 7, 9, 23, 88, 43]
list1 = numberlist
#passing by reference vs. passing by value
list2 = numberlist[:]
list2.sort()
print list1
print list2

#data manipulation
print '2nd nominee', icebucket_nominees[1]

print 'first 2 entries', icebucket_nominees[0:2]
print 'last 2 entries', icebucket_nominees[-2:]


#logic
print 88 in numberlist



