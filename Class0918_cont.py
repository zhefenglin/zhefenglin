import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob

#gauss filter on delta function
im = np.zeros((100,100))
im[4,6] = 1
blurry_im = ndimage.gaussian_filter(im,2)
print im
print blurry_im

#plt.figure(figsize = (10,5))
#plt.subplot(121)
#plt.imshow(im, cmap=plt.cm.gray)
#plt.axis('off')
#plt.subplot(122)
#plt.imshow(blurry_im)
#plt.axis('off')
#plt.show()

#draw a square
sq_im = np.zeros((256,256))
sq_im[64:-64,64:-64] = 1

plt.figure(figsize = (10,5))
plt.subplot(121)
plt.imshow(sq_im, cmap=plt.cm.gray, interpolation = 'none')
plt.axis('off')

plt.show()