# Mini Project 1
import math
import matplotlib.pyplot as plt

# global variables
gg = 9.81 # m/s**2
dT = 1e-4 # s
TT = 0 # s

# Model
ddz = -gg

# initial value
zz = 0 # m
xx = 0 # m

vv0 = 5 # m/s
aa0 = 45 # degree (From 0 to 180)

dx = vv0 * math.cos(math.radians(aa0))
dz = vv0 * math.sin(math.radians(aa0))

# initial state
data = [(TT, xx, zz, dx, dz )]
X_pos = []
Z_pos = []

# add element as tuple to a list
def addelement(A, a, b, c, d, e):
	B= (a, b, c, d, e)
	if len(A) >=1000:
		del A[0]
	A.append(B)
	return A

# devide the elements in the tuple
def data_process(data):
        T=[]
        X=[]
        Z=[]
        V_x=[]
        V_z=[]
        for status in data:
                T.append(status[0])
                X.append(status[1])
                Z.append(status[2])
                V_x.append(status[3])
                V_z.append(status[4])
        return T, X, Z, V_x, V_z

# find median
def find_median(A):
        if len(A) % 2 == 1:
                A_median=A[(len(A)-1)/2]
        else:
                A_median=(A[(len(A)/2-1)]+A[len(A)/2])/2
        return A_median

# find standard deviation
def find_std(A,A_average):
        sqr_sum=0
        for i in A:
                sqr_sum=sqr_sum+(i-A_average)**2
        return (sqr_sum/len(A))**0.5
 
 # find the max, min, mean, median and stdDev       
def stat_comp(A):
        A.sort()
        A_max = max(A)
        A_min = min(A)
        A_average = sum(A)/len(A)
        A_median = find_median(A)
        A_stdDev = find_std(A,A_average)
        A_stat = [A_max,A_min,A_average,A_median,A_stdDev]
        return A_stat
 
 # find the newest and oldest value       
def get_data(data):
        newest = data[-1]
        oldest = data[0]
        return newest, oldest


# solver
while zz >= 0 :
        X_pos.append(xx)
        Z_pos.append(zz)
        zz = zz + dz * dT + 0.5 * ddz * dT ** 2
	xx = dx * dT + xx
	dz = dz + ddz * dT
	TT = TT + dT
	data = addelement(data, TT, xx, zz, dx, dz)

 #when the ball hits the ground
        #if zz < 0:
                #zz = 0
                #dz = -dz
 #stop the loop after time = 10 s (otherwise the program will not stop)
        #if TT > 10:
                #break



T, X, Z, V_x, V_z = data_process(data)

# plot the last 1000 points
plt.plot(X_pos,Z_pos)
plt.xlabel('X axis')
plt.ylabel('Z axis')
plt.title('projectile (stop when hits the ground)')

# find the statastical value of data

X_newest, X_oldest = get_data(X)
Z_newest, Z_oldest = get_data(Z)
V_x_newest, V_x_oldest = get_data(V_x)
V_z_newest, V_z_oldest = get_data(V_z)

X_stat = stat_comp(X)
Z_stat = stat_comp(Z)
V_x_stat = stat_comp(V_x)
V_z_stat = stat_comp(V_z)
# Note that now the order of X, Z, V_x and V_z are changed!!

#print data
print 'The maximun, minimum, mean, median and standard deviation of X are', X_stat
print 'The maximun, minimum, mean, median and standard deviation of Z are', Z_stat
print 'The maximun, minimum, mean, median and standard deviation of dx are', V_x_stat
print 'The maximun, minimum, mean, median and standard deviation of dz are' , V_z_stat
print 'The newest and oldest of X are', X_newest,'and', X_oldest
print 'The newest and oldest of Z are', Z_newest, 'and', Z_oldest
print 'The newest and oldest of dx are', V_x_newest, 'and', V_x_oldest
print 'The newest and oldest of dz are', V_z_newest, 'and', V_z_oldest

# show the plot
plt.show()