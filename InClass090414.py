# ball falling
import matplotlib.pyplot as plt
import turtle
import time
# global variables
gg = 9.81 # m/s^2
TT = 0.01

#model
ddz = -gg

#solver 1 time
#dz0 = 10 # m/s
#z0 = 1 # m
#dz = dz0 + ddz * TT
#zz = z0 + dz * TT + 0.5 * ddz * TT**2
#print zz


#initial values
dz = [10]
zz = [1]

#solver
while zz[-1] > 0:
	dz.append( dz[-1] + ddz * TT)
	zz.append( zz[-1] + dz[-1] * TT + 0.5 * ddz * TT ** 2)
plt.plot(zz)
plt.xlabel('iteration')
plt.ylabel('[m]')
plt.title('falling ball')


#file data management
plt.subplots_adjust(wspace = 0.4)
plt.subplots_adjust(hspace = 0.6)
plt.savefig('fallingball.png')
plt.show()


# animation
win = turtle.Screen()
win.bgcolor("white")
tess = turtle.Turtle()
tess.shape("turtle")
tess.color("blue")
tess.shapesize(5,5,2)
tess.pencolor("white")

# move turtle to initial position
tess.sety( zz[0] * 50)
tess.penup()
tess.left(90)
ss = tess.stamp()
time.sleep(1)
aa = zz[0]
# iteration
# draw each position of the turtle
for item in zz:
	tess.sety( item *50)
time.sleep(5)